FROM ubuntu:latest

RUN apt update && apt upgrade && apt install \
	npm nodejs 

RUN npm i mqtt

WORKDIR home

ENTRYPOINT /bin/bash
